#include <iostream>
#include <fstream>
#include <vector>
#include "bank_operations.h"
#include "file_reader.h"
#include <algorithm>
#include "sorts.h"



int main()
{
    std::cout << "Laboratory work #9. GIT\n";
    std::cout << "Variant #7. Bank operation\n";
    std::cout << "Author: Kupchin Danila\n";
    std::cout << "Group: 16\n";

    setlocale(LC_ALL, "Rus");

    int n = 0;
    std::string file_name;
    file_name = "data.txt";
    Data data[5];
    std::ifstream in(file_name); // окрываем файл для чтения
    if (in.is_open())
    {
        Data buff;
        while (in >> buff.date >> buff.time >> buff.type >> buff.account >> buff.sum >> buff.goal)
        {
            data[n++] = buff;
        }
    }
    in.close();

    for (int i = 0; i < n; i++)
    {
        std::cout << "Date - " << data[i].date << " Time - " << data[i].time << " Type - " << data[i].type << " Bank account " << data[i].account << " Operation sum " << data[i].sum << " Goal - " << data[i].goal << std::endl;
    }



    std::cout << "Choose type of sorting \n - 1 for show all incoming transactions\n - 2 for show all transactions of november 2021:\n";
    int choise_1, choise_2;
    std::cin >> choise_1;
    if (choise_1 == 1)
    {
        std::cout << "Output all incoming transactions:\n";
        sort1(data);
    }
    else if (choise_1 == 2)
    {
        std::cout << "Output all transactions of november:\n";
        sort2(data);
    }


    bool (*comp[])(Data, Data) = { *compare_1, *compare_2 };
    choise_1 = 0;
    std::cout << "Choose type of sort :\n - 0 Quick sort\n - 1 Merge sort\n";
    std::cin >> choise_1;
    std::cout << "How it should be sorted:\
                  \n - 0 On icreasing of transaction goal\
                  \n - 1 On increasing of bank account(if account of transaction the same sort on increasing of type next on reducing sum\n";
    std::cin >> choise_2;
    if (choise_1 == 0)
    {
        std::sort(data, data + 5, comp[choise_2]);
    }
    else if (choise_1 == 1)
    {
        choicesSort(data, 5, comp[choise_2]);
    }
    for (int i = 0; i <= 5; i++)
        show_data(data, i);
    return 0;

}

