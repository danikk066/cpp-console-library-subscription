#ifndef SORT_HEADER_H
#define SORT_HEADER_H
#include <fstream>
#include <iostream>
#include "bank_operations.h"


void sort1(Data* data)
{
	for (int i = 0; i < 5; i++)
	{
		if (data[i].type == "Prichod")
		{
			show_data(data, i);
		}
	}
}

void sort2(Data* data)
{
	for (int i = 0; i < 5; i++)
	{
		if (data[i].date[3] == '1' && data[i].date[4] == '1' && data[i].date[8] == '2' && data[i].date[9] == '1')
		{
			show_data(data, i);
		}
	}
}

bool compare_1(Data a, Data b)
{
	return a.goal > b.goal;
}
bool compare_2(Data a, Data b)
{
	if (a.account == b.account)
	{
		if (a.type == b.type)
		{
			return a.sum < b.sum;
		}
		else return a.type > b.type;
	}
	else return a.account < b.account;
}
void choicesSort(Data* arrayPtr, int length_array, bool comp(Data a, Data b)) // сортировка выбором
{
	for (int repeat_counter = 0; repeat_counter < length_array; repeat_counter++)
	{
		auto temp = arrayPtr[0]; // временная переменная для хранения значения перестановки
		for (int element_counter = repeat_counter + 1; element_counter < length_array; element_counter++)
		{
			if (!comp(arrayPtr[repeat_counter], arrayPtr[element_counter]))
			{
				temp = arrayPtr[repeat_counter];
				arrayPtr[repeat_counter] = arrayPtr[element_counter];
				arrayPtr[element_counter] = temp;
			}
		}
	}
}
#endif