#ifndef BANK_OPERATION_H
#define BANK_OPERATION_H
#include <string>

#include "constants.h"

struct Data
{
    std::string date;
    std::string time;
    std::string type;
    int  account;
    int sum;
    std::string goal;

};

#endif // !BANK_OPERATION_H


